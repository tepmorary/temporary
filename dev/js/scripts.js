var swiper = new Swiper('.swiper-container', {
    pagination: '.swiper-pagination',
    loop: true,
    nextButton: '.swiper-button-next',
    prevButton: '.swiper-button-prev',
    spaceBetween: 30,
    effect: 'fade',
    // autoplay: 2500,
    autoplayDisableOnInteraction: false
});
$(document).ready(function() {
	$('.map__btn').click(function(event) {
		$('#map').removeClass('notShow');
	});
	$('.map__close').click(function(event) {
		$('#map').addClass('notShow');
	});
});
function scrollTod(id) {
	event.preventDefault();
	var to = $('#'+id).offset().top;
	$("html, body").animate({scrollTop: to}, 500);
}
$(function() {
    if($('.navbar').length > 0) {
        $('.navbar').css('z-index', 1000);
        var menu = $('.navbar').offset().top;
        $(window).scroll(function() {
            if ($(this).scrollTop() > menu) {
                if ($('.navbar').css('position') != 'fixed') {
                    $('.navbar').css({
                        'position': 'fixed',
                        'top': '0',
                        'left': '0',
                        'width': '100%',
                        'margin-top': '0',
                        'box-shadow': '0px 0px 10px rgba(0,0,0,.4)'
                    });
                    $('.navbar-header .header__phones').removeClass('hidden')
                }
            } else {
                if($('.navbar').css('position') != 'static') {
                    $('.navbar').css({
                        'position': 'static',
                        'margin-top': '20px',
                        'box-shadow': 'none'
                    });
                    $('.navbar-header .header__phones').addClass('hidden')
                }
            }
        });
    }
});